= POIs

- Ein POI ist eine Zeile in der Tabelle Points
  - Die (implizite) rowid ist der Primary Key
  - id ist die OpenStreetMap-ID dieses Knotens bzw. Pfades
  - type ist P oder W, je nachdem, ob das zugehörige OpenStreetMap-Objekt ein Knoten oder ein Pfad ist
  - name ist der Name des POIs
  - geom ist eine Koordinate (POINT), die die Position dieses POIs darstellt.
  - Auch wenn type W ist, ist geom immer ein Punkt, nie eine kompliziertere Geometrie
  - Beispiel:
      Way: Feringasee (3401802); https://www.openstreetmap.org/way/3401802
      ist dargestellt als 3401802; W; <geometry>

- Zu einem POI können zusätzliche Informationen als Tags abgelegt werden. Dazu dienen die drei Tabellen
  Points_Key_Value, TagKeys und TagValues. Die Tags zu einem POI mit rowid <x> werden bestimmt mit der Query

  select tk.name key, tv.name value
  from Points_Key_Value pks
  join TagKeys tk on pks.TagKeys_id = tk.id
  join TagValues tv on pks.TagValues_id = tv.id
  where pks.points_id = <x>

  Einige Keys und Values haben besondere Bedeutungen:
  - Einträge mit dem Schlüssel "url" werden in LocusMaps unter "Links" angezeigt.
  - Der Eintrag "Weitere Informationen in OpenStreetMap" wird automatisch generiert und entsteht nicht aus einem Tag
  - Teilweise scheint LocusMap Übersetzungen zu enthalten. Z.B. wird für das K/V-Paar "sport" / "swimming" in der
    deutschen Oberfläche "Sport" / "Schwimmen" angezeigt. "natural" / "water" wird jedoch nicht übersetzt.
  
- POIs können in eine zweistufige Hierarchie einsortiert werden. Dazu dienen die drei Tabellen
  Points_Root_Sub, FoldersRoot und FoldersSub. Die Hierarchie zu einem POI mit rowid <x> wird bestimmt mit der Query

  select fr.name root, fs.name sub
  from Points_Root_Sub prs
  join FoldersRoot fr on prs.FoldersRoot_id = fr.id
  join FoldersSub fs on prs.FoldersSub_id = fs.id
  where prs.points_id = <x>

  Das in der Oberfläche angezeigte Icon ergibt sich aus der Root/Sub-Kombination

  Die Namen in FolrdersRoot und FoldersSub sind englisch, deutsche Übersetzungen & Icons scheinen fest in der LocusMap
  verdrahtet zu sein. Es können beliebige Namen hinzugefügt werden; zu unbekannten Namen fehlt allerdings dann sowohl
  die deutsche Übersetzung als auch ein sinnvolles Icon (es wird ein (i) als Standard-Icon genutzt).




- Ein POI ist ein Eintrag in Points mit einer ID, einem Type ('P' oder 'W'), einem Namen und einer Koordinate
  - ID scheint die ID des POIs in OpenStreetMap zu sein
- Der Fremdschlüssel für Points_Key_Value.points_id ist Points.rowid
- Der Fremdschlüssel für Points_Root_Sub.points_id ist Points.rowid
- Beispiel:

  select P.*  from points P  where P.name like '%feringasee%';
  --> 2 rows returned
  155127193; P; Gasthof Feringasee
  3401802; W; Feringasee

  Die ID ist die ID in OpenStreetMap

  Sucht man in Locus nach "Feringasee" findet man diese POIs. Zum zweiten gibt es auch eine Reihe an
  Metainformationen, z.B. "Wikipedia": "De:Feringasee".

  select * from TagValues where name like "de:feringasee";
  --> 199221; de:Feringasee

  select * from TagKeys where name like "Wikipedia"
  --> "33"	"wikipedia"

  select * from points_key_value where tagvalues_id = 199221 and tagkeys_id = 33;
  --> "636504"	"33"	"199221"

  select * from points where rowid = 636504;
  --> "3401802"	"W"	"Feringasee"

  D.h. irgendwo gibt es (versteckt?) einen Bezug zwischen 3401802 und 636504


- Es gibt 920555 Punkte in meiner Version der Karte
  
  select count(*) from points
  --> 920555



== Tags
- URLs werden in Locus in der POI-Ansicht unter "Links" dargestellt.
- TODO: Ist eine URL über die TagKeys.id (37) oder den TagKeys.name ("url") identifiziert?
- Einige Begriffe scheinen in der Anwendung direkt übersetzt zu werden. Beispiel:

  select pkv.points_id,  pkv.tagkeys_id , tk.name TK_name, pkv.tagvalues_id, tv.name TV_name from points_key_value pkv join tagkeys tk on pkv.tagkeys_id = tk.id join tagvalues tv on pkv.tagvalues_id = tv.id where  points_id = 636504;
  -->
  "636504"	"31"	"sport"	"116"	"swimming"
  "636504"	"8"	"natural"	"17619"	"water"
  "636504"	"37"	"url"	"199220"	"http://www.landkreis-muenchen.de/bildung-kultur-freizeit-sport/freizeit/badegewaesser-und-badeseen/feringasee/"
  "636504"	"33"	"wikipedia"	"199221"	"de:Feringasee"

  Statt "swimming" wird in der Detailansicht zu diesem Punkt (Feringasee) "Schwimmen" angezeigt. "water" wird aber als "Water" angezeigt.


TODO
- Was bedeuten die Typen P, W bei Points?
- Hypothese:
  - P: Point
  - W: Way
  (3401802 hat type W und ist in OpenStreetMap ein Way)
  - Auch bei type=W ist geom ein Punkt
  - Ggfs. nur notwendig um die OSM-Url zu konstruieren?

