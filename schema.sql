CREATE TABLE FoldersRoot (id INTEGER NOT NULL PRIMARY KEY,name TEXT NOT NULL);
CREATE TABLE FoldersSub (id INTEGER NOT NULL PRIMARY KEY,name TEXT NOT NULL);

CREATE TABLE TagKeys (id INTEGER NOT NULL PRIMARY KEY,name TEXT NOT NULL);
CREATE TABLE TagValues (id INTEGER NOT NULL PRIMARY KEY,name TEXT NOT NULL);

CREATE TABLE Points_Root_Sub (Points_id INTEGER NOT NULL,FoldersRoot_id INTEGER NOT NULL,FoldersSub_id INTEGER NOT NULL);
CREATE TABLE Points_Key_Value (Points_id INTEGER NOT NULL,TagKeys_id INTEGER NOT NULL,TagValues_id INTEGER NOT NULL);
CREATE TABLE Points (id INTEGER NOT NULL,type TEXT NOT NULL,name TEXT);
SELECT AddGeometryColumn('Points', 'geom', 4326, 'POINT', 'XY');
SELECT CreateSpatialIndex('Points', 'geom');

CREATE TABLE Regions (id BIGINT NOT NULL PRIMARY KEY);
SELECT AddGeometryColumn('Regions', 'geom', 4326, 'MULTIPOLYGON', 'XY');
SELECT CreateSpatialIndex('Regions', 'geom');
CREATE TABLE Regions_Names (regionid BIGINT NOT NULL,langcode TEXT NOT NULL, name TEXT, namenorm TEXT NOT NULL);

CREATE TABLE Cities (id BIGINT NOT NULL PRIMARY KEY,type INT NOT NULL, parentcityid BIGINT, regionid BIGINT, lon INT, lat INT);
SELECT AddGeometryColumn('Cities', 'center', 4326, 'POINT', 'XY');
SELECT AddGeometryColumn('Cities', 'geom', 4326, 'MULTIPOLYGON', 'XY');
SELECT CreateSpatialIndex('Cities', 'geom');

CREATE TABLE Cities_Names (cityid BIGINT NOT NULL,langcode TEXT NOT NULL, name TEXT, namenorm TEXT NOT NULL);
CREATE VIEW View_Cities_Def_Names AS SELECT Cities.ROWID as ROWID, Cities.id, Cities_Names.name, Cities_Names.namenorm, Cities.geom FROM Cities JOIN Cities_Names ON Cities.id = Cities_Names.cityid WHERE Cities_Names.langcode = 'def';
INSERT INTO views_geometry_columns (view_name, view_geometry, view_rowid, f_table_name, f_geometry_column, read_only)
  VALUES ('view_cities_def_names', 'geom', 'rowid', 'cities', 'geom', 1);

CREATE TABLE Streets (id INT NOT NULL PRIMARY KEY,name ,namenorm TEXT NOT NULL, data BLOB);
SELECT AddGeometryColumn('Streets', 'geom', 4326, 'MULTILINESTRING', 'XY');
SELECT CreateSpatialIndex('Streets', 'geom');
CREATE TABLE Street_In_Cities (streetid INT NOT NULL,cityid BIGINT NOT NULL);

CREATE TABLE Postcodes (id INTEGER PRIMARY KEY, postcode TEXT);

CREATE TABLE MetaData (id TEXT PRIMARY KEY,value TEXT);

-- Create indices
CREATE INDEX idx_cities_lon_lat ON Cities (lon, lat);
CREATE INDEX idx_cities_names_cityid ON Cities_Names (cityid);
CREATE INDEX idx_cities_names_namenorm ON Cities_Names (namenorm, langcode);
CREATE INDEX idx_pkv_points_id ON Points_Key_Value (Points_id);
CREATE INDEX idx_prs_points_id ON Points_Root_Sub (Points_id);
CREATE INDEX idx_prs_root ON Points_Root_Sub (FoldersRoot_id);
CREATE INDEX idx_prs_root_sub ON Points_Root_Sub (FoldersRoot_id, FoldersSub_id);
CREATE INDEX idx_regions_names_cityid ON Regions_Names (regionid);
CREATE INDEX idx_regions_names_langcode ON Regions_Names (langcode);
-- Called 'idx_strees_in_cities_cityid' in LoMaps
CREATE INDEX idx_streets_in_cities_cityid ON Street_In_Cities (cityid);
-- Called 'idx_strees_in_cities_streetid' in LoMaps
CREATE INDEX idx_streets_in_cities_streetid ON Street_In_Cities (streetid);
CREATE INDEX idx_streets_namenorm ON Streets (namenorm);
